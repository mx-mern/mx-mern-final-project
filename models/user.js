const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    projects: [
        {
            type: Schema.Types.ObjectId,
            ref: 'project'
        }
    ]
})


// Create a model from that schema
const User = mongoose.model('user', userSchema);

// create a model's map to export
let userModel = {}

userModel.createuser = function (newuser, cb) {
    var user = new User(newuser);
    user.save(function (err, data) {
        cb(err, data);
    })
}

userModel.getUserByEmail = function (data, cb) {
    User.findOne({email: data.email}).exec(function (err, data) {
        // if (err) {
        //     console.log("Cannot find user with email" + email);
        // } else {
        //     console.log("User found " + data);
        // }
        cb(err, data);
    });
}

module.exports = userModel;


