const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const itemSchema = new Schema({
    device_id: {
        type: String,
        required: true,
        unique: true
    },
    device_name: {
        type: String,
        required: true
    },
    device_type: {
        type: String,
        required: true
    },
    icon: {
        type: String,
        required: true
    },
    update_method: {
        type: String,
        enum: ['MQTT', 'REST'],
        default: 'MQTT'
    },
    group_id: {
        type: Schema.Types.ObjectId,
        ref: 'group'
    },
    project_id: {
        type: Schema.Types.ObjectId,
        ref: 'project'
    }
})


// Create a model from that schema
const Item = mongoose.model('item', itemSchema);

// create a model's map to export
let itemModel = {}

itemModel.createItem = function (newitem, cb) {
    var item = new Item(newitem);
    item.save(function (err, data) {
        cb(err, data);
    })
}

itemModel.getItemById = function (id, cb) {
    Item.findOne({ device_id: id }).exec(function (err, data) {
        cb(err, data);
    });
}

itemModel.getItem = function (cb) {
    Item.find().exec(function (err, data) {
        cb(err, data);
    });
}

itemModel.updateItem = function (updatedItem, cb) {
    console.log(updatedItem)
    Item.findOneAndUpdate( updatedItem.device_id, updatedItem, function (err, data) {
        cb(err, data);
    })  
}

module.exports = itemModel;


