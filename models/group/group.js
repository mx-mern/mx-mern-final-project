const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const groupSchema = new Schema({
    group_name: {
        type: String,
        required: true
    },
    item_id: [{
        type: Schema.Types.ObjectId,
        ref: 'items'
    }],
    project_id: {
        type: Schema.Types.ObjectId,
        ref: 'project'
    }
})


// Create a model from that schema
const Group = mongoose.model('group', groupSchema);

// create a model's map to export
let groupModel = {}

groupModel.createGroup = function (newgroup, cb) {
    var group = new Group(newgroup);
    group.save(function (err, data) {
        cb(err, data);
    })
}

groupModel.getGroup = function (email, cb) {
    Group.find().exec(function (err, data) {
        cb(err, data);
    });
}

module.exports = groupModel;


