const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const projectSchema = new Schema({
    name: {
        type: String
    },
    setup: {
        type: String
    },
    permissions: [
        {
            user: {
                type: Schema.Types.ObjectId,
                ref: 'user'
            },
            role: String
        }
    ]
})


// Create a model from that schema
const Project = mongoose.model('project', projectSchema)

// create a model's map to export
let projectModel = {}

projectModel.findAll = function (cb) {
    Project.find({}).exec(function(err, data){
        cb(err, data)
    })
}

projectModel.createProject = function (cb) {
    var project = new Project()
    project.save(function (err, data) {
        cb(err, data)
    })
}

projectModel.getProjectById = function (id, cb) {
    Project.findById(id).exec(function (err, data) {
        cb(err, data)
    })
}

projectModel.setProjectName = function (data, cb) {
    Project.findByIdAndUpdate(data.id, { name: data.name }).exec(function (err, data) {
        cb(err, data)
    })
}

projectModel.setProjectSetup = function (data, cb) {
    console.log(`setProjectSetup id: ${data.id}, setup: ${JSON.stringify(data.setup)}`)
    Project.findByIdAndUpdate(data.id, { setup: JSON.stringify(data.setup) }).exec(function (err, data) {
        cb(err, data)
    })
}
console.log("PROJECT-MODEL exported")
module.exports = projectModel;


