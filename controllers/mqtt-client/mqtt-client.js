const projectModel = require('../../models/project')
const mqtt = require('mqtt')
// create a model's map to export
let mqttClient = {}


mqttClient.updateProject = (projectId, setup) => {
    let oldProjectSetup = null
    if (mqttClient[projectId]) {
        oldProjectSetup = mqttClient[projectId].setup
        console.log(`Project is updated from ${JSON.stringify(oldProjectSetup)} to ${JSON.stringify(setup)}`)
        mqttClient[projectId].client.end()
    } else {
        mqttClient[projectId] = {}
    }
    if (setup && setup.connectionType === "MQTT") {
        mqttClient[projectId] = {}
        mqttClient[projectId].setup = setup
        let client = mqtt.connect(setup.mqttHost, {port: setup.mqttPort})
        mqttClient[projectId].client = client
        // let topic = setup.mqttRootPath
        client.on('connect', () => {
            console.log('mqtt connected to: ' + setup.mqttHost + ', subcribed: ' + setup.mqttRootPath + ' on port ' + setup.mqttPort)
            client.subscribe(setup.mqttRootPath)
        })

        client.on('message', (topic, message) => {
            if (topic === setup.mqttRootPath) {
                connected = (message.toString() === 'true');
                console.log(`received message from  [${setup.mqttHost}:${setup.mqttPort}] topic [${setup.mqttRootPath}] ` + message.toString())
                //TODO: handle message from these topics and dispatch or update items in mongodb
            }
        })

        client.on('disconnect', () => {
            console.log(`disconnect from [${setup.mqttHost}:${setup.mqttPort}]`)
        })

        client.on('end', () => {
            console.log(`end connection from [${setup.mqttHost}:${setup.mqttPort}]`)
        })

    }
}

projectModel.findAll(function (err, projects) {
    if (projects) {
        //console.log("mqttClient.projects length: " + mqttClient.projects.length + ": " + JSON.stringify(mqttClient.projects))
        let i = 0
        var project
        for (project of projects) {
            let projectId = project._id
            let setup = project.setup ? JSON.parse(project.setup) : null
            let connected = false
            // console.log
            mqttClient.updateProject(projectId, setup)
        }
        // console.log('mqttClient: ' + JSON.stringify(mqttClient))
    }
})
console.log("MQTT-CLIENT exported")
module.exports = mqttClient;


