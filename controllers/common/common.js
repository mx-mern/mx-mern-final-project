const common = {};
common.checkAuth = (req, res, next) => {
    if (!req.session.email) {
        console.log('common.checkAuth req.session.email does not exists!!!' + req.session.email)
        res.status(401)
        res.json({
            success: false,
            email: null,
            message: "Please login"
        })
    } else {
        next();
    }
  }
module.exports = common;