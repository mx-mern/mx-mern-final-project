const express = require('express')
const project = require('./project')
const common = require('../common/common')

const router = express.Router()

router.use(common.checkAuth)

/**
 * @swagger
 * /project/setup:
 *  post:   
 *      tags:   
 *          - API TAG
 *      name: Api to get a project using id
 *      summary: Api find project by id
 *      consumes:
 *          - application/json
 *      parameters:
 *          -   in: body
 *              name: body
 *              description: project setup info
 *              required: true
 *              schema:
 *                  type: object
 *                  properties: 
 *                       id: 
 *                          type: string
 *                          required: true
 *                          example: 5e1b1fae1f40428645b9dfa8
 *                       setup: 
 *                          type: string
 *                          required: true
 *                          example: {"host": "localhost", "port": 1883} 
 *      responses:
 *              - default: 
 *                  description: successful operation
 *                  type: object 
 */
router.route('/setup').post(project.setSetup)
/**
 * @swagger
 * /project/info:
 *  get:   
 *      tags:   
 *          - API TAG
 *      name: Api to get a project using id
 *      summary: Api find project by id
 *      consumes:
 *          - application/json
 *      parameters:
 *          -   in: query
 *              name: id
 *              description: projectid
 *              required: true
 *              schema:
 *                  type: string
 *                  example: 5e1b1fae1f40428645b9dfa8
 *      responses:
 *              - default: 
 *                  description: successful operation
 *                  type: object 
 */
router.route('/info').get(project.getProjectInfo)
console.log('module.exports = project-router')
module.exports = router;