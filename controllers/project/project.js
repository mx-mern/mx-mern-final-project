const express = require('express')
const projectModel = require('../../models/project')
const mqttClient = require('../mqtt-client/mqtt-client')

const projRouter = express.Router()

projRouter.setSetup = function(req, res) {
    // if (!req.session.email) {
    //     console.log('req.session.email does not exists, response.data = undefined !!!' + req.session.email)
    //     res.json({
    //         success: false,
    //         email: null,
    //         message: "Please login"
    //     })
    // } else {
        if (req.body.id && req.body.setup) {
            console.log("setup for project " + req.body.id + ": " + req.body.setup)
            projectModel.setProjectSetup(req.body, function (err, data) {
                if (err) {
                    res.json({
                        success: false,
                        message: "Project not found with errors",
                        errors: err.errors
                    })
                } else {
                    if (data) {
                        res.json({
                            message: "Project setup is updated successfully ",
                            success: true,
                            data: data
                        })
                        // Notify update to mqttClient 
                        mqttClient.updateProject(req.body.id, req.body.setup)
                    } else {
                        res.json({
                            success: false,
                            message: "Project updated setup failed"
                        })
                    }
                }
            })
        } else {
            res.json({
                success: false,
                message: "Invalide request"
            })
        }
    // }
}

projRouter.getProjectInfo = function (req, res) {
    console.log('getProjectInfo , req.session.email' + req.session.email)
    // if (!req.session.email) {
    //     console.log('getProjectInfo req.session.email does not exists!!!' + req.session.email)
    //     res.json({
    //         success: false,
    //         email: null,
    //         message: "Please login"
    //     })
    // } else {
        projectModel.getProjectById(
            req.query.id, function (err, data) {
            if (err) {
                res.json({
                    success: false,
                    message: "Project not found with errors",
                    errors: err.errors
                })
            } else {
                if (data) {
                    res.json({
                        message: "Project Get Info OK ",
                        success: true,
                        data: data
                    })
                } else {
                    res.json({
                        success: false,
                        message: "Project not found"
                    })
                }
            }
        })
    // }
}
module.exports = projRouter;