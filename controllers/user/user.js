const express = require('express')
const bcrypt = require('bcrypt')
const userModel = require('../../models/user')
const projectModel = require('../../models/project')
const userAuth = express.Router()


const saltRounds = 10;

userAuth.register = function (req, res) {
    if (!req.body.email || !req.body.password) {
        res.json({
            success: false,
            message: "Email & password are required!"
        })
    } else {
        userModel.getUserByEmail(req.body, function (err, data) {
            if (data != null) {
                res.json({
                    success: false,
                    message: `email ${data.email} exists!!!`
                })
            } else {
                bcrypt.genSalt(saltRounds, function (err, salt) {
                    if (err) {
                        res.json({
                            success: false,
                            message: "Something went wrong 1"
                        })
                        return;
                    } else {
                        bcrypt.hash(req.body.password, salt, function (err, hash) {
                            if (err) {
                                res.json({
                                    success: false,
                                    message: "Something went wrong 2"
                                })
                            } else {
                                userModel.createuser({
                                    name: req.body.name,
                                    email: req.body.email,
                                    password: hash
                                }, function (err, data) {
                                    if (err) {
                                        console.log("Cannot create UserModel " + {
                                            name: req.body.name,
                                            email: req.body.email,
                                            password: hash
                                        })
                                        res.json({
                                            message: err.errors,
                                            success: false
                                        })
                                    } else {
                                        res.json({
                                            message: 'Save success',
                                            email: data.email,
                                            password: data.password,
                                            success: true
                                        })
                                    }
                                })
                            }
                        })
                    }
                })
            }
        })
    }
}
userAuth.login = function (req, res) {
    if (!req.body.email || !req.body.password) {
        res.json({
            success: false,
            message: "Email & password are required"
        })
    } else {
        userModel.getUserByEmail(req.body, function (err, user) {
            if (err || user == null) {
                res.json({
                    success: false,
                    message: "Email not found"
                })
            } else {
                bcrypt.compare(req.body.password, user.password, function (err, result) {
                    if (err) {
                        res.json({
                            success: false,
                            message: "Password are not found"
                        })
                    } else {
                        if (result == true) {

                            req.session.email = user.email;
                            req.session.permision = user.permissions;
                            req.session.save()

                            userdata = Object.assign({}, user.toObject())
                            delete userdata.password
                            delete userdata.__v
                            // If there's no project, create a project 
                            if (user.projects.length == 0) {
                                projectModel.createProject(function (err, project) {
                                    if (err) {
                                        console.log("project create failed")
                                    } else {
                                        console.log("project created: " + project)
                                        user.projects.push(project._id)
                                        project.permissions.push({ user: user._id, role: 'admin' })
                                        project.save(function (err, data) {
                                            if (err) {
                                                console.log("update project permisison failed")
                                                res.json({
                                                    message: "Login successfuly, no project found",
                                                    success: true,
                                                    result: result,
                                                    data: userdata
                                                })
                                            } else {
                                                console.log("create project OK: " + project._id)
                                                user.save(function (err, data) {
                                                    if (err) {
                                                        console.log('Update user project failed')
                                                        res.json({
                                                            message: "Login successfuly, no project found",
                                                            success: true,
                                                            result: result,
                                                            data: userdata
                                                        })
                                                    } else {
                                                        userdata.projects.push(project._id)
                                                        console.log("Add project to userdat OK: " + project._id)
                                                        res.json({
                                                            message: "Login successfuly",
                                                            success: true,
                                                            result: result,
                                                            data: userdata
                                                        })
                                                    }
                                                })
                                            }
                                        })
                                    }
                                })
                            } else {
                                console.log("Login successfully, projects length != 0")
                                res.json({
                                    message: "Login successfuly",
                                    success: true,
                                    result: result,
                                    data: userdata
                                })
                            }
                        } else {
                            res.json({
                                message: "Login failed",
                                success: false,
                                result: result
                            })
                            console.log("password is not correct")
                        }
                    }
                })
            }
        })
    }
}

userAuth.getUserInfo = function (req, res) {
    console.log('userAuth.getUserInfo , req.session.email' + req.session.email)
    if (!req.session.email) {
        console.log('req.session.email does not exists, response.data = undefined !!!' + req.session.email)
        res.json({
            success: false,
            email: null,
            message: "Please login"
        })
    } else {
        userModel.getUserByEmail({
            email: req.session.email
        }, function (err, data) {
            if (err) {
                res.json({
                    success: false,
                    message: "Email not found with errors",
                    errors: err.errors
                })
            } else {
                if (data) {
                    data.password = undefined;
                    res.json({
                        message: "Email registered ",
                        success: true,
                        data: data
                    })
                } else {
                    res.json({
                        success: false,
                        message: "Email not found"
                    })
                }
            }
        })
    }
}



userAuth.findByEmail = function (req, res) {
    let email = req.query.email;

    // console.log("req.session.email:" + JSON.stringify(req.session))

    if (!req.session.email) {
        res.json({
            success: false,
            email: null,
            message: "Please login"
        })
    } else {
        if (email) {
            userModel.getUserByEmail({
                email: email
            }, function (err, data) {
                if (err) {
                    res.json({
                        success: false,
                        message: "Email not found with errors",
                        errors: err.errors
                    })
                } else {
                    if (data) {
                        data.password = undefined;
                        res.json({
                            message: "Email registered ",
                            success: true,
                            data: data
                            // email: data.email,
                            // name: data.name,
                            // projects: data.projects
                        })
                    } else {
                        res.json({
                            success: false,
                            message: "Email not found"
                        })
                    }
                }
            })
        } else {
            res.json({
                success: false,
                message: "Missing email query param"
            })
        }
    }
}

userAuth.logout = function (req, res) {
    console.log("Destroyed req.session: " + JSON.stringify(req.session))
    req.session.destroy(err => {
        res.status(200).end()
    })
}

module.exports = userAuth;