const express = require('express')
const userAuth = require('./user')
const common = require('../common/common')

const router = express.Router()
/**
 * @swagger
 * /user/register:
 *  post:   
 *      tags:   
 *          - API TAG
 *      name: Api to register a user
 *      summary: Api create user
 *      consumes:
 *          - application/json
 *      parameters:
 *          -   name: body
 *              in: body
 *              description: new user info
 *              required: true
 *              schema:
 *                  type: object
 *                  properties: 
 *                       name: 
 *                          type: string
 *                          required: true
 *                          example: Nguyen Tien Thinh
 *                       email: 
 *                          type: string
 *                          required: true
 *                          example: tienthinh2011@gmail.com 
 *                       password: 
 *                          type: string
 *                          required: true
 *                          example: 123456aA@
 * 
 *      responses:
 *              - default: 
 *                  description: successful operation
 *                  type: object 
 */
router.route('/register').post(userAuth.register)
/**
 * @swagger
 * /user/login:
 *  post:   
 *      tags:   
 *          - API TAG
 *      name: Api to login with email as user & password
 *      summary: Api login
 *      consumes:
 *          - application/json
 *      parameters:
 *          -   name: body
 *              in: body
 *              description: new user info
 *              required: true
 *              schema:
 *                  type: object
 *                  properties:
 *                       email: 
 *                          type: string
 *                          required: true
 *                          example: tienthinh2011@gmail.com 
 *                       password: 
 *                          type: string
 *                          required: true
 *                          example: 123456aA@
 * 
 *      responses:
 *              - default: 
 *                  description: successful operation
 *                  type: object 
 */
router.route('/login').post(userAuth.login)
/**
 * @swagger
 * /user/logout:
 *  post:   
 *      tags:   
 *          - API TAG
 *      name: Api to logout
 *      summary: Api logout
 *      consumes:
 *          - application/json
 *      responses:
 *              - default: 
 *                  description: successful operation
 *                  type: object 
 */
router.route('/logout').post(userAuth.logout)
/**
 * @swagger
 * /user/findByEmail:
 *  get:   
 *      tags:   
 *          - API TAG
 *      name: Api to get a user using email
 *      summary: Api find user by email
 *      consumes:
 *          - application/json
 *      parameters:
 *          -   in: query
 *              name: email
 *              description: user email
 *              required: true
 *              schema:
 *                  type: string
 *                  format: email
 *                  example: tienthinh2011@gmail.com
 *      responses:
 *              - default: 
 *                  description: successful operation
 *                  type: object 
 */
router.route('/findByEmail').get(userAuth.findByEmail)

/**
 * @swagger
 * /user/info:
 *  get:   
 *      tags:   
 *          - API TAG
 *      name: Api to get a user using email
 *      summary: Api find user by email
 *      responses:
 *              - default: 
 *                  description: successful operation
 *                  type: object 
 */
router.route('/info').get(userAuth.getUserInfo)

module.exports = router;