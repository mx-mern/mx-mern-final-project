const express = require('express');
const itemModel = require('../../models/item/item');
const itemAuth = express.Router();

itemAuth.createItem = function (req, res) {
    if (!req.body.device_id || !req.body.device_name || !req.body.device_type || !req.body.icon) {
        res.json({
            success: false,
            message: "Lack required field"
        })
    } else {
        itemModel.getItemById(req.body.device_id, function (err, data) {
            if (data != null) {
                res.json({
                    success: false,
                    message: `id already exists!!!`
                })
            } else {
                if (err) {
                    res.json({
                        success: false,
                        message: err
                    })
                    return;
                } else {
                    itemModel.createItem({
                        device_id: req.body.device_id,
                        device_name: req.body.device_name,
                        device_type: req.body.device_type,
                        icon: req.body.icon,
                        update_method: req.body.update_method
                    }, function (err, data) {
                        if (err) {
                            res.json({
                                message: err.errors,
                                success: false
                            });
                        } else {
                            res.json({
                                message: 'Save success',
                                data: data,
                                success: true
                            });
                        }
                    });
                }
            }
        });
    }
}

itemAuth.findAllItem = function (req, res) {
    itemModel.getItem(function (err, data) {
        if (err) {
            res.json({
                success: false,
                message: "No item found",
                errors: err.errors
            })
        } else {
            if (data) {
                res.json({
                    success: true,
                    data: data
                })
            } else {
                res.json({
                    success: false,
                    message: "No item found"
                })
            }
        }
    })
}

itemAuth.updateItem = function (req, res) {
    console.log(req.body)
    itemModel.getItemById(req.params.id, function (err, data) {
        console.log(err, data)
        if (err) {
            res.json({
                success: false,
                message: err
            })
            return;
        } else {
            itemModel.updateItem(req.body, function (err, data) {
                if (err) {
                    res.json({
                        message: err.errors,
                        success: false
                    });
                } else {
                    res.json({
                        message: 'Save success',
                        data: data,
                        success: true
                    });
                }
            });
        }
    })
}

itemAuth.deleteItem = function (req, res) {
    console.log(req.params.id)
}

itemAuth.findItemById = function (req, res) {
    itemModel.getItemById(req.params.id, function (err, data) {
        if (err) {
            res.json({
                success: false,
                message: "No item found",
                errors: err.errors
            })
        } else {
            if (data) {
                res.json({
                    success: true,
                    data: data
                })
            } else {
                res.json({
                    success: false,
                    message: "No item found"
                })
            }
        }
    })
}

module.exports = itemAuth;