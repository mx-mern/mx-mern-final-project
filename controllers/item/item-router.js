const express = require('express');
const itemAuth = require('./item');
const common = require('../common/common')

const router = express.Router();
router.use(common.checkAuth)
/**
 * @swagger
 * /item/register:
 *  post:   
 *      tags:   
 *          - API TAG
 *      name: Api to register a item
 *      summary: Api create item
 *      consumes:
 *          - application/json
 *      parameters:
 *          -   name: body
 *              in: body
 *              description: new item info
 *              required: true
 *              schema:
 *                  type: object
 *                  properties: 
 *                       device_id:
 *                          type: string
 *                          required: true
 *                          example: ab:sa:cx:12:sa:r3
 *                       device_name:
 *                          type: string
 *                          required: true
 *                          example: den
 *                       device_type:
 *                          type: string
 *                          required: true
 *                          example: den
 *                       icon:
 *                          type: string
 *                          required: true
 *                          example: den
 *                       update_method:
 *                          type: string
 *                          required: true
 *                          example: MQTT
 * 
 *      responses:
 *              - default: 
 *                  description: successful operation
 *                  type: object 
 */
router.route('/').post(itemAuth.createItem);

/**
 * @swagger
 * /item/findByEmail:
 *  get:   
 *      tags:   
 *          - API TAG
 *      name: Api to get a item using id
 *      summary: Api find item by id
 *      consumes:
 *          - application/json
 *      parameters:
 *          -   in: query
 *              name: id
 *              description: item id
 *              required: true
 *              schema:
 *                  type: string
 *                  format: id
 *                  example: ab:sa:cx:12:sa:r3
 *      responses:
 *              - default: 
 *                  description: successful operation
 *                  type: object 
 */
router.route('/').get(itemAuth.findAllItem);


router.route('/:id')
    .get(itemAuth.findItemById)
    .put(itemAuth.updateItem)
    .delete(itemAuth.deleteItem);

module.exports = router;