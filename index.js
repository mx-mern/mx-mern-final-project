const express = require('express')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const session = require('express-session')
const user = require('./controllers/user/user-router')
const item = require('./controllers/item/item-router')
const project = require('./controllers/project/project-router')
const config = require('./config/config')
const common = require('./controllers/common/common')
const path = require('path');
const app = express()

app.set('trust proxy', 1) // trust first proxy
app.use(session({
  secret: 'keyboard cat',
  resave: false,
  saveUninitialized: true,
  cookie: { 
    // Preferred way to set Expires attribute. Time in milliseconds until
    // the expiry. There's no default, so the cookie is non-persistent.
    maxAge: 1000 * 60 * 60 * 2,
    // Secure attribute in Set-Cookie header. Whether the cookie can ONLY be
    // sent over HTTPS. Can be set to true, false, or 'auto'. Default is false.
    secure: false 
  }
}))

/* ====================================
    SET HEADER
=====================================*/
app.use(function(req, res, next) {
  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000')

  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE')

  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type')

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader('Access-Control-Allow-Credentials', true)
  next()
})

// parse application/x-www-form-urlencoded
// app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.urlencoded())
// parse application/json
app.use(bodyParser.json())

mongoose.connect(config.mongoUrl, {useNewUrlParser: true})

let db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'))
db.once('open', function() {
    console.log('connected!!!')
})

// swagger
var swagger = require('./swagger/swagger')
swagger(app)

// app.use(express.static('views'))
if (config.env === 'prod') {
  app.use('/', express.static(path.join(__dirname, 'public/build')))
}
app.use('/user', user)
app.use('/item', item)
app.use('/project', project)

let mqttClient = require('./controllers/mqtt-client/mqtt-client')
app.get('*', (req,res) => res.sendFile(path.join(__dirname+'/public/build/index.html'))).listen(process.env.PORT || 8000,() => console.log('Server on port ' + process.env.PORT || 8000))
// app.listen(process.env.PORT || 8000, function() {
//     console.log('server listening on port 8000')
// })
